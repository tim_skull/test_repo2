let http = require("http");

const responder = (request, response) => {
    const body = 'hello world\n';
    response.writeHead(200, {
        'Content-Length': Buffer.byteLength(body),
        'Content-Type': 'text/plain'
    });
    response.end(body);
};

http.createServer(responder).listen(3000);

console.log('Server running');
